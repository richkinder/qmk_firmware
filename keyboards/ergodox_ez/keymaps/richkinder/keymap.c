#include QMK_KEYBOARD_H
#include "debug.h"
#include "action_layer.h"
#include "version.h"

#define COLEMAK 0
#define QWERTY 1
#define EXTRAS 2

enum custom_keycodes {
  PLACEHOLDER = SAFE_RANGE, // can always be here
  EPRM,
  VRSN,
  RGB_SLD
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* Keymap 0: Colemak
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |   1  |   2  |   3  |   4  |   5  |      |           |      |   6  |   7  |   8  |   9  |   0  |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |    `   |   Q  |   W  |   F  |   P  |   G  |  L1  |           |  L2  |   J  |   L  |   U  |   Y  |   ;  |   \    |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |    =   |GUI|A |Shft|R|Alt|S |Ctl|T |Lext|D|------|           |------|   H  |Ctl|N |Alt|E |Shft|I|GUI|O |   '    |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |   Z  |   X  |   C  |   V  |   B  |      |           |      |   K  |   M  |   ,  |   .  |   /  |   -    |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |   [  |      |      |   ]  |                                       | Left | Down |  Up  | Right|      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      | Home |       | PgUp |  Esc   |
 *                                 ,------|------|------|       |------+--------+------.
 *                                 |      |      | End  |       | PgDn |        |      |
 *                                 | Space|Backsp|------|       |------|  Tab   |Enter |
 *                                 |      |ace   | Del  |       |      |        |      |
 *                                 `--------------------'       `----------------------'
 */

// If it accepts an argument (i.e, is a function), it doesn't need KC_.
// Otherwise, it needs KC_*
[COLEMAK] = LAYOUT_ergodox(  // layer 0 : Colemak
// left hand    |                |                |                |                |                |
_______,         KC_1,            KC_2,            KC_3,            KC_4,            KC_5,            _______,
KC_GRV,          KC_Q,            KC_W,            KC_F,            KC_P,            KC_G,            TG(QWERTY),
KC_EQL,          LGUI_T(KC_A),    LSFT_T(KC_R),    LALT_T(KC_S),    LCTL_T(KC_T),    LT(EXTRAS,KC_D),
_______,         KC_Z,            KC_X,            KC_C,            KC_V,            KC_B,            _______,
_______,         KC_LBRC,         _______,         _______,         KC_RBRC,
                                                                                     _______,         KC_HOME,
                                                                                                      KC_END,
                                                                    KC_SPC,          KC_BSPC,         KC_DEL,
// right hand   |                |                |                |                |                |
_______,         KC_6,            KC_7,            KC_8,            KC_9,            KC_0,            _______,
TG(EXTRAS),      KC_J,            KC_L,            KC_U,            KC_Y,            KC_SCLN,         KC_BSLS,
                 KC_H,            LCTL_T(KC_N),    RALT_T(KC_E),    RSFT_T(KC_I),    RGUI_T(KC_O),    KC_QUOT,
_______,         KC_K,            KC_M,            KC_COMM,         KC_DOT,          KC_SLSH,         KC_MINS,
                                  KC_LEFT,         KC_DOWN,         KC_UP,           KC_RGHT,         _______,
KC_PGUP,         KC_ESC,
KC_PGDN,
_______,         KC_TAB,          KC_ENT
    ),

/* Keymap 1: QWERTY


 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |   E  |   R  |   T  |      |           |      |   Y  |   U  |   I  |   O  |   P  |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |GUI|A |Shft|S|Alt|D |Ctl|F |   G  |------|           |------|      |Ctl|J |Alt|K |Shft|L|GUI|; |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |   N  |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *   |      |      |      |      |      |                                       |      |      |      |      |      |
 *   `----------------------------------'                                       `----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |      |      |       |      |        |
 *                                 ,------|------|------|       |------+--------+------.
 *                                 |      |      |      |       |      |        |      |
 *                                 |      |      |------|       |------|        |      |
 *                                 |      |      |      |       |      |        |      |
 *                                 `--------------------'       `----------------------'
 */
// If it accepts an argument (i.e, is a function), it doesn't need KC_.
// Otherwise, it needs KC_*
[QWERTY] = LAYOUT_ergodox(  // layer 1 : qwerty
// left hand    |                |                |                |                |                |
_______,         _______,         _______,         _______,         _______,         _______,         _______,
_______,         _______,         _______,         KC_E,            KC_R,            KC_T,            _______,
_______,         LGUI_T(KC_A),    LSFT_T(KC_S),    LALT_T(KC_D),    LCTL_T(KC_F),    LT(EXTRAS,KC_G),
_______,         _______,         _______,         _______,         _______,         _______,         _______,
_______,         _______,         _______,         _______,         _______,
                                                                                     _______,         _______,
                                                                                                      _______,
                                                                    _______,         _______,         _______,
// right hand   |                |                |                |                |                |
_______,         _______,         _______,         _______,         _______,         _______,         _______,
_______,         KC_Y,            KC_U,            KC_I,            KC_O,            KC_P,            _______,
                 _______,         LCTL_T(KC_J),    RALT_T(KC_K),    RSFT_T(KC_L),    RGUI_T(KC_SCLN), _______,
_______,         KC_N,            _______,         _______,         _______,         _______,         _______,
                                  _______,         _______,         _______,         _______,         _______,
_______,         _______,
_______,
_______,         _______,         _______
	),

/* Keymap 2: Extras
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |Version |  F1  |  F2  |  F3  |  F4  |  F5  |      |           |      |  F6  |  F7  |  F8  |  F9  |  F10 |   F11  |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |Bright|Bright|      |      |   F12  |
 * |        |      |      |      |      |      |      |           |      |      |ness- |ness+ |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |MsAcl2|MsAcl1|MsAcl0|      |------|           |------|      |MsLeft|MsDown| MsUp |MsRght|        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      | Mute | Vol- | Vol+ |      |     	 |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------' 
 *   |      |      |      |      |      |                                       |      |      |      |      |      |   
 *   `----------------------------------'                                       `----------------------------------'   
 *                                        ,-------------.       ,-------------.										   
 *                                        |Animat|      |       |Toggle|Solid |										   
 *                                 ,------|------|------|       |------+------+------.								   
 *                                 |      |      |      |       |      |   	  |    	 |								   
 *                                 | Lclk |	   	 |------|       |------|      | Rclk 								   
 *                                 |      |      |      |       |      |      |      |								   
 *                                 `--------------------'       `--------------------'								   
 */																					 								   
// If it accepts an argument (i.e, is a function), it doesn't need KC_.				 								   
// Otherwise, it needs KC_*															 								   
[EXTRAS] = LAYOUT_ergodox(  // layer 2 : extras
// left hand    |                |                |                |                |                |
VRSN,            KC_F1,           KC_F2,           KC_F3,           KC_F4,           KC_F5,           _______,
_______,         _______,         _______,         _______,         _______,         _______,         _______,
_______,         _______,         KC_MS_ACCEL2,    KC_MS_ACCEL1,    KC_MS_ACCEL0,    _______,
_______,         _______,         _______,         _______,         _______,         _______,         _______,
_______,         _______,         _______,         _______,         _______,
                                                                                     RGB_MOD,         _______,
                                                                                                      _______,
                                                                    KC_BTN1,         _______,         _______,
// right hand   |                |                |                |                |                |
_______,         KC_F6,           KC_F7,           KC_F8,           KC_F9,           KC_F10,          KC_F11,
_______,         _______,         RGB_VAD,         RGB_VAI,         _______,         _______,         KC_F12,
                 _______,         KC_MS_L,         KC_MS_D,         KC_MS_U,         KC_MS_R,         _______,
_______,         _______,         KC_MUTE,         KC_VOLD,         KC_VOLU,         _______,         _______,
                                  _______,         _______,         _______,         _______,         _______,
RGB_TOG,         RGB_SLD,		  									
_______,
_______,         _______,         KC_BTN2							
	),

};

const uint16_t PROGMEM fn_actions[] = {
    [1] = ACTION_LAYER_TAP_TOGGLE(QWERTY)                // FN1 - Momentary Layer 1 (Symbols)
};

const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt)
{
  // MACRODOWN only works in this function
  switch(id) {
    case 0:
      if (record->event.pressed) {
        SEND_STRING (QMK_KEYBOARD "/" QMK_KEYMAP " @ " QMK_VERSION);
      }
      break;
    case 1:
      if (record->event.pressed) { // For resetting EEPROM
        eeconfig_init();
      }
      break;
  }
  return MACRO_NONE;
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    // dynamically generate these.
    case EPRM:
      if (record->event.pressed) {
        eeconfig_init();
      }
      return false;
      break;
    case VRSN:
      if (record->event.pressed) {
        SEND_STRING (QMK_KEYBOARD "/" QMK_KEYMAP " @ " QMK_VERSION);
      }
      return false;
      break;
    case RGB_SLD:
      if (record->event.pressed) {
        #ifdef RGBLIGHT_ENABLE
          rgblight_mode(1);
        #endif
      }
      return false;
      break;
  }
  return true;
}

// Runs just one time when the keyboard initializes.
void matrix_init_user(void) {
#ifdef RGBLIGHT_COLOR_LAYER_0
  rgblight_setrgb(RGBLIGHT_COLOR_LAYER_0);
#endif
};

// Runs constantly in the background, in a loop.
void matrix_scan_user(void) {

};

// Runs whenever there is a layer state change.
uint32_t layer_state_set_user(uint32_t state) {
  ergodox_board_led_off();
  ergodox_right_led_1_off();
  ergodox_right_led_2_off();
  ergodox_right_led_3_off();

  // Layer mask is 1, but layer 0 is always on.
  uint32_t layer_1_mask = 2;
  uint32_t layer_2_mask = 4;
  uint32_t layer_3_mask = 8;
  if (state & layer_1_mask) {
  	  ergodox_right_led_1_on();
  }
  if (state & layer_2_mask) {
  	  ergodox_right_led_2_on();
  }
  if (state & layer_3_mask) {
  	  ergodox_right_led_3_on();
  }

  /* uint8_t layer = biton32(state); */
  /* switch (layer) { */
  /*     case 0: */
  /*       #ifdef RGBLIGHT_COLOR_LAYER_0 */
  /*         rgblight_setrgb(RGBLIGHT_COLOR_LAYER_0); */
  /*       #else */
  /*       #ifdef RGBLIGHT_ENABLE */
  /*         rgblight_init(); */
  /*       #endif */
  /*       #endif */
  /*       break; */
  /*     case 1: */
  /*       ergodox_right_led_1_on(); */
  /*       #ifdef RGBLIGHT_COLOR_LAYER_1 */
  /*         rgblight_setrgb(RGBLIGHT_COLOR_LAYER_1); */
  /*       #endif */
  /*       break; */
  /*     case 2: */
  /*       ergodox_right_led_2_on(); */
  /*       #ifdef RGBLIGHT_COLOR_LAYER_2 */
  /*         rgblight_setrgb(RGBLIGHT_COLOR_LAYER_2); */
  /*       #endif */
  /*       break; */
  /*     case 3: */
  /*       ergodox_right_led_3_on(); */
  /*       #ifdef RGBLIGHT_COLOR_LAYER_3 */
  /*         rgblight_setrgb(RGBLIGHT_COLOR_LAYER_3); */
  /*       #endif */
  /*       break; */
  /*     case 4: */
  /*       ergodox_right_led_1_on(); */
  /*       ergodox_right_led_2_on(); */
  /*       #ifdef RGBLIGHT_COLOR_LAYER_4 */
  /*         rgblight_setrgb(RGBLIGHT_COLOR_LAYER_4); */
  /*       #endif */
  /*       break; */
  /*     case 5: */
  /*       ergodox_right_led_1_on(); */
  /*       ergodox_right_led_3_on(); */
  /*       #ifdef RGBLIGHT_COLOR_LAYER_5 */
  /*         rgblight_setrgb(RGBLIGHT_COLOR_LAYER_5); */
  /*       #endif */
  /*       break; */
  /*     case 6: */
  /*       ergodox_right_led_2_on(); */
  /*       ergodox_right_led_3_on(); */
  /*       #ifdef RGBLIGHT_COLOR_LAYER_6 */
  /*         rgblight_setrgb(RGBLIGHT_COLOR_LAYER_6); */
  /*       #endif */
  /*       break; */
  /*     case 7: */
  /*       ergodox_right_led_1_on(); */
  /*       ergodox_right_led_2_on(); */
  /*       ergodox_right_led_3_on(); */
  /*       #ifdef RGBLIGHT_COLOR_LAYER_7 */
  /*         rgblight_setrgb(RGBLIGHT_COLOR_LAYER_6); */
  /*       #endif */
  /*       break; */
  /*     default: */
  /*       break; */
  /*   } */

  return state;
};
